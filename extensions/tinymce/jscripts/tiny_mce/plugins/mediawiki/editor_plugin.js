/**
 * $Id$
 *
 * @author Bret McMillan <bretm@redhat.com>
 * @copyright Copyright � 2004-2008, Red Hat, Inc., All rights reserved.
 *
 * Adapted from:
 * - the "example" and "bbcode" plugins provided by Moxicode
 * - Remy Sharp's wiki2html code: http://remysharp.com/2008/04/01/wiki-to-html-using-javascript/
 * 
 * See also:  http://en.wikipedia.org/wiki/Wikipedia:Cheatsheet
 */

(function() {
	tinymce.create('tinymce.plugins.MediaWikiPlugin', {
		/**
		 * Initializes the plugin, this will be executed after the plugin has been created.
		 * This call is done before the editor instance has finished it's initialization so use the onInit event
		 * of the editor instance to intercept that event.
		 *
		 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
		 * @param {string} url Absolute URL to where the plugin is located.
		 */
		init : function(ed, url) {
                        var t = this;
                        // load wiki2html syncronous calls...
                        // might want to explore async later
                        tinymce.ScriptLoader.load(tinymce.PluginManager.urls.mediawiki + '/wiki2html.js');

			ed.onBeforeSetContent.add(function(ed, o) {
				o.content = t['_mw2html'](o.content);
			});

			ed.onPostProcess.add(function(ed, o) {
				if (o.set)
					o.content = t['_mw2html'](o.content);
				
				if (o.get)
					o.content = t['_html2mw'](o.content);
				
			});
		},

		/**
		 * Returns information about the plugin as a name/value array.
		 * The current keys are longname, author, authorurl, infourl and version.
		 *
		 * @return {Object} Name/value array containing information about the plugin.
		 */
		getInfo : function() {
			return {
				longname : 'MediaWiki plugin',
				author : 'Bret McMillan <bretm@redhat.com>',
				authorurl : 'http://www.redhat.com/',
				infourl : 'http://www.redhat.com/',
				version : "0.1"
			};
		},

		// Private methods

                convertlists : function(s) {
		    var t = this;

		    s = s.replace(/<(?:ol|ul)>(.*?)<\/(?:ol|ul)>/gim, function (m) {
			    var glyph = m.match(/ol/) ? '#' : '*';

			    m.replace(/<li>(.*?)<\/li>/gim, glyph + "$1");

			    // TODO:  fixme, this needs to be recursive...
			    m = t['convertlists'](m);

			    return m;
			});

		    return s;
                },

		// HTML -> MediaWiki, it'd be nice to get this upstream into remy's code
		_html2mw : function(s) {
		        var t = this;

			function rep(re, str) {
				s = s.replace(re, str);
			};

			// WikiWord urls
			rep(/<a.*?href=\"([A-Z].*?)\".*?>\1<\/a>/gim, "[[$1]]");

			// handle where url's text is the same as the url
			rep(/<a.*?href=\"(.*?)\".*?>\1<\/a>/gim, "$1");


			// handle external urls with body text
			rep(/<a.*?href=\"(.+?)\".*?>(.+?)<\/a>/gim,"[$1 $2]");


			// images
			// XXXXXXXXXXXXXXXXXXXXXX


			// <em> to ''
			rep(/<em>(.*?)<\/em>/gim, "''$1''");

			// <strong> to '''
			rep(/<strong>(.*?)<\/strong>/gim, "'''$1'''");

			// headers
			rep(/<h1>(.*?)<\/h1>/gim, "=$1=");
			rep(/<h2>(.*?)<\/h2>/gim, "==$1==");
			rep(/<h3>(.*?)<\/h3>/gim, "===$1===");
			rep(/<h4>(.*?)<\/h4>/gim, "====$1====");
			rep(/<h5>(.*?)<\/h5>/gim, "=====$1=====");
			rep(/<h6>(.*?)<\/h6>/gim, "======$1======");


			// block elements, spacing, etc...

			// <p>
			//rep(/<p>([\s\S]*?)<\/p>/gim, '$1\n');
			// <p align>
			//rep(/<p.*?align\: left.*?>([\s\S]*?)<\/p>/gim, '\n$1');
			// various indents...
			// <p style="padding-left"...>
			//rep(/<p .*?padding\-left.*?>([\s\S]*?)<\/p>/gim, '\n  $1');
			//rep(/<pre>([\s\S]*?)<\/pre>/gim, '\n $1');
			//rep(/<blockquote>([\s\S]*?)<\/blockquote>/gim, '\n  $1');
			// <br>
			//rep(/<br[^>]*?>/gim, '\n');

			// ul,ol lists
			// s = t['convertlists'](s);

			// tables
			// XXXXXXXXXXXXXXXXXXXXXX
			// this doesn't work great atm ... :/
                        rep(/<table.*?>.*?(?:<tbody>)([\s\S]*?)<\/tbody>.*?<\/table>/gim, "{|\n$1|}\n");
                        rep(/<tr.*?>\s*?([\s\S]*?)\s*?<\/tr>/gim, "|-\n$1");
                        rep(/<td.*?>[\s\n]*?([^\n]*?)[\n\s]*?<\/td>/gim, "|$1\n");
			// this last one 'cause my regex's seem to leave <td/>'s all over the place...
			// probably due to a tinymce cleanup function repairing bad html
			// rep(/\s*?<td\/>\s*?/gim, "");

			// <nbsp>
			//rep(/\&nbsp\;/gim, ' ');

			return s;
		},

		// MediaWiki -> HTML, delegate to remy's code
		_mw2html : function(s) {
		    //s = tinymce.trim(s);
			return s.wiki2html();
		}
	});

	// Register plugin
	tinymce.PluginManager.add('mediawiki', tinymce.plugins.MediaWikiPlugin);
})();