<?php
/*
TinyMCE_MW2.php - MediaWiki extension - version 0.1

Bret McMillan <bretm@redhat.com>
Copyright 2008, Red Hat, Inc., All rights reserved.

Rewritten from Joseph Socoloski's original tinymce extension;
moves logic to the tinymce plugin framework

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/


if( !defined( 'MEDIAWIKI' ) ) {
  die();
}

$wgExtensionCredits['other'][] = array(

        "name" => "Next-gen TinyMCE MediaWiki extension",
        "author" => "Bret McMillan <bretm@redhat.com>",
        "version" => "0.1",
        "url" => "http://www.redhat.com/",
        "description" => "Easily implement Moxiecode's TinyMCE into MediaWiki using the plugin framework (rework of Joseph Socoloski's original work)"
);

# REGISTER HOOKS
$wgHooks['ArticleAfterFetchContent'][]          = 'wfCheckBeforeEdit';
$wgHooks['EditPage::showEditForm:initial'][]    = 'wfTinymceAddScript';

function wfTinymceAddScript($q) {
  global $wgOut, $wgScriptPath;

  # use the more modern example straight from moxiecode
  $wgOut->addScript("<script language=\"javascript\" type=\"text/javascript\">
function toggleEditor(id) {
	if (!tinyMCE.get(id))
		tinyMCE.execCommand('mceAddControl', false, id);
	else
		tinyMCE.execCommand('mceRemoveControl', false, id);
}
</script>");

  $wgOut->addScript( "<script language=\"javascript\" type=\"text/javascript\" src=\"$wgScriptPath/extensions/tinymce/jscripts/tiny_mce/tiny_mce.js\"></script><script language=\"javascript\" type=\"text/javascript\">tinyMCE.init({
        mode : \"textareas\",
        theme : \"advanced\",
        plugins : \"safari,inlinepopups,spellchecker,paste,media,fullscreen,table,mediawiki\",
	entity_encoding : \"raw\",
        cleanup : true,
	add_unload_trigger : false,
	remove_linebreaks : false,
        convert_newlines_to_brs : false,
        force_p_newlines : false,
        force_br_newlines : false,
        forced_root_block : '',
	inline_styles : false,
	convert_fonts_to_spans : true,
        apply_source_formatting : false,
	theme_advanced_buttons1 : \"bold,italic,strikethrough,underline,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,|,outdent,indent,|,link,unlink,|,fullscreen\",
        theme_advanced_buttons2 : \"formatselect,forecolor,|,image,media,charmap,|,undo,redo,|,code\",
        theme_advanced_buttons3 : \"\",
        theme_advanced_buttons4 : \"\",
	theme_advanced_toolbar_location : \"top\",
	theme_advanced_toolbar_align : \"left\",
	theme_advanced_statusbar_location : \"bottom\",
	theme_advanced_resizing : true,
        document_base_url : \"$wgMyWikiURL\"});</script>"
			   );
  # Since editing add the button
  $wgOut->addHTML("<p><a href=\"javascript:toggleEditor('wpTextbox1');\" title=\"toggle wysiwyg editor\">Toggle Visual Editor</a></p>");
  return true;
}

# Check existing article for any tags we don't want TinyMCE parsing...
function wfCheckBeforeEdit ($q, $text) { 
	global $wgUseTinymce;
	
	if (preg_match("|&lt;(data.*?)&gt;(.*?)&lt;/data&gt;|is", $text, $a)) {
		$wgUseTinymce = false;
	}
	elseif(preg_match("|<(data.*?)>(.*?)</data>|is", $text, $a)) {
		$wgUseTinymce = false;}
	else{$wgUseTinymce = true;}
	return true;
}

?>

